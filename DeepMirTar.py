"""

input: miRNA.fa, mature miRNA, 3' --> 5'
       mRNA.fa, 3'UTR region of mRNA, 3' --> 5'

       e.g. miRNA.fa
       ****************************************
       *** > hsa-miR-495-3p                 ***
       *** AUCAACAGACAUUAAUUGGGCGC          ***
       ***                                  ***
       *** > hsa-miR-29c-3p                 ***
       *** UAGCACCAUUUGAAAUCGGUUA           ***
       ***                                  ***
       ****************************************

       e.g. mRNA.fa
       ****************************************
       *** > NM_001135811                   ***
       *** CUGAGGUUUUUAUGUAGAAGGGGAACAAAAAA ***
       *** AAAAAUAUCUGAAUUUUGAAAAACCACAAAGC ***
       *** UACAAACUGACCCUCUUUUUUUUUUGAGACGG ***
       *** AGUUUUGCUCUUGUUACCCAGGCUGGAGUGCA ***
       *** GUGG...                           ***
       ***                                  ***
       *** > NM_001846                      ***
       *** GCCGGCGCGUGCCAGGAAGGGCCAUUUUGGUG ***
       *** CUUAUUCUUAACUUAUUACCUCAGGUGCCAAC ***
       *** CCAAAAAUUGGUUUUA...              ***
       ***                                  ***
       ****************************************

output: miRNA-mRNA.tspr <tspr means target site prediction results>

       e.g. miRNA-mRNA.tspr
       *********************************************************
       *** > hsa-miR-495-3p|NM_001135811                     ***
       *** target site:  no hits found                       ***
       ***                                                   ***
       *** > hsa-miR-495-3p|NM_001846                        ***
       *** target site:  no hits found                       ***
       ***                                                   ***
       *** > hsa-miR-29c-3p|NM_001135811                     ***
       *** target site:  no hits found                       ***
       ***                                                   ***
       *** > hsa-miR-29c-3p|NM_001846                        ***
       *** target location: [17, 34]; ...                    ***
       *** binding type: canonical; ...                      ***
       *** miRNA:  AUUGGCUAAAGUUUACCACGAU , 3' --> 5' ; ...  ***
       *** target: AGGGCCAUUU----UGGUGCUU , 5' --> 3' ; ...  ***
       ***                                                   ***
       *********************************************************

notes: no blank lines between the id line and the sequence line.
for example, the form below is not allowed.

       ****************************************
       *** > hsa-miR-495-3p                 ***
       ***                                  ***<-- this blank line is NOT ALLOWED !!!
       *** AUCAACAGACAUUAAUUGGGCGC          ***
       ***                                  ***
       *** > hsa-miR-29c-3p                 ***
       *** UAGCACCAUUUGAAAUCGGUUA           ***
       ***                                  ***
       ****************************************


"""

import os, getopt, sys
import pickle
import tensorflow as tf
import time
import numpy
from RNA_features_site_level import get_all_RNA_site_level_features


def standardize_seq(seq):  # 1, standardize seq: upper, and replace T with U
    seq_s = seq.upper().replace('T', 'U')
    for base in seq_s:
        if base not in ['A', 'U', 'G', 'C']:
            print('Unknown character(s) in sequence except AUGC')
            break
    return seq_s


def read_fasta_file(fasta_file):  # convert fasta file to dic
    f = open(fasta_file)
    data = f.readlines()
    f.close()

    data_dic = {}
    record = []
    for i in range(len(data)):
        if data[i].startswith('>'):
            record.append(i)
    for i in range(len(record))[0:-1]:
        loc = record[i]
        loc_next = record[i+1]
        key = data[loc].replace('>', '').strip()
        value = ''
        for line in data[loc+1: loc_next]:
            value += line.strip()
        data_dic[key] = standardize_seq(value)

    key = data[record[-1]].replace('>', '').strip()  # the last block
    value = ''
    for line in data[record[-1]+1:]:
        value += line.strip()
    data_dic[key] = standardize_seq(value)
    return data_dic


def complementary_type(seq1, seq2):
    count_c = 0
    count_w = 0
    c = ['AU', 'UA', 'GC', 'CG']
    w = ['GU', 'UG']
    for i in range(len(seq1)):
        ss = seq1[i] + seq2[i]
        if ss in c:
            count_c += 1
        elif ss in w:
            count_w += 1
    if count_c == 6:
        result = 'canonical'
    elif count_c + count_w >= 5:
        result = 'non-canonical'
    else:
        result = 'NT'
    return result


def extract_cts_from_miranda_file(miranda_file):
    cts = {}

    f = open(miranda_file)
    mr = f.readlines()
    f.close()

    info_all = []
    query_all = []
    ref_all = []

    for line in mr:
        if line.strip().startswith('Query:'):
            query_all.append(line.strip())
        if line.strip().startswith('Ref:'):
            ref_all.append(line.strip())
        if line.startswith('>'):
            info_all.append(line.strip())
    for i in range(len(query_all)):
        query = query_all[i]
        ref = ref_all[i]
        info = info_all[i]

        seed27 = query[-10:-4]
        seed27_site = ref[-10:-4]

        seed38 = query[-11:-5]
        seed38_site = ref[-11:-5]

        type27 = complementary_type(seed27, seed27_site)
        type38 = complementary_type(seed38, seed38_site)
        if type27 == 'canonical' or type38 == 'canonical':
            result = 'canonical'
            miRNA_binding = query.split()[2]
            mRNA_binding = ref.split()[2]
            mRNA_loc = [int(info.split()[6]), int(info.split()[7])]
            cts[str(i)] = {'miRNA35': miRNA_binding,
                           'mRNA53': mRNA_binding,
                           'mRNA_loc': mRNA_loc,
                           'binding_type': result}
        elif type27 == 'non-canonical' or type38 == 'non-canonical':
            result = 'non-canonical'
            miRNA_binding = query.split()[2]
            mRNA_binding = ref.split()[2]
            mRNA_loc = [int(info.split()[6]), int(info.split()[7])]
            cts[str(i)] = {'miRNA35': miRNA_binding,
                           'mRNA53': mRNA_binding,
                           'mRNA_loc': mRNA_loc,
                           'binding_type': result}
    return cts


def screen_canc_cts(miRNA_seq, mRNA_seq):  #  Screen candidate target sites (CTS) by using mirnada program
                                                    #  canc means canonical and non-canonical cts
    f = open('./miRNA_miranda.fa', 'w')
    f.write('>miRNA_miranda' + '\n')
    f.write(miRNA_seq + '\n')
    f.close()

    f = open('./mRNA_miranda.fa', 'w')
    f.write('>mRNA_miranda' + '\n')
    f.write(mRNA_seq + '\n')
    f.close()

    abspath = os.getcwd()

    os.system('miranda '
              '%s/miRNA_miranda.fa '
              '%s/mRNA_miranda.fa '
              '-en 10000 '
              '-sc 60 '
              '-out %s/miRNA_mRNA.miranda' % (abspath, abspath, abspath))
    cts = extract_cts_from_miranda_file('./miRNA_mRNA.miranda')
    return cts


def get_features(cts, mRNA_seq):
    miRNA_binding = cts['miRNA35']
    mRNA_binding = cts['mRNA53']
    miRNA_loc = cts['mRNA_loc']
    rsf = get_all_RNA_site_level_features(miRNA_binding, mRNA_binding, mRNA_seq, miRNA_loc, flank_number=70)
    return rsf


def Prediciton(cts, mRNA_seq, SdA, MinMaxScaler):
    Feature = MinMaxScaler.transform(numpy.asmatrix(list(get_features(cts, mRNA_seq).values()), dtype='float32'))
    predicted_results = SdA.predict(Feature)
    return predicted_results


def main():
    # parsing parameters
    try:
        opts, args = getopt.getopt(sys.argv[1:], "i:m:o:")
    except getopt.GetoptError as err:
        print(str(err))
        print('Usage: python3 DeepMirTar.py -i miRNA.fa -m mRNA.fa -o output')  # will print something like "option -a not recognized"
        sys.exit(2)

    for i in opts:
        if i[0] == '-i':
            miRNA_file = i[1]
        if i[0] == '-m':
            mRNA_file = i[1]
        if i[0] == '-o':
            output_file = i[1]

    f = open('./source/MinMaxScaler.pkl', 'rb')
    MinMaxScaler = pickle.load(f, encoding='iso-8859-1')
    f.close()

    SdA = tf.keras.models.load_model('./sda.h5')

    miRNA_dic = read_fasta_file(miRNA_file)
    mRNA_dic = read_fasta_file(mRNA_file)

    CTS_dic = {}
    for key_miRNA in miRNA_dic.keys():
        for key_mRNA in mRNA_dic.keys():
            miRNA_seq = miRNA_dic[key_miRNA]
            mRNA_seq = mRNA_dic[key_mRNA]
            CTS_dic[key_miRNA + '|' + key_mRNA] = screen_canc_cts(miRNA_seq, mRNA_seq)  # cts_location, miRNA_seq, mRNA_seq,
            # complementary model by miranda
    #os.remove(abspath_source + '/miRNA_miranda.fa')
    #os.remove(abspath_source + '/mRNA_miranda.fa')
    #os.remove(abspath_source + '/miRNA_mRNA.miranda')

    print(CTS_dic.keys())

    f = open(output_file, 'w')
    
    for key in CTS_dic.keys():
        f.write('> ' + key + '\n')
        if len(CTS_dic[key]) == 0:
            f.write('target site:  no hits found' + '\n')
        else:
            number_hits = 0
            for cts_key in CTS_dic[key].keys():
                cts = CTS_dic[key][cts_key]
                predicted_results = Prediciton(cts, mRNA_dic[key.split('|')[1]], SdA, MinMaxScaler)
                print(predicted_results)
                if predicted_results[0][0] > 0.5:
                    number_hits += 1
                    f.write('Target location: ' + str(cts['mRNA_loc'][0]) + ',' + str(cts['mRNA_loc'][1]) + '\n')
                    f.write('Type:' + cts['binding_type'] + '\n')
                    f.write('miRNA:  ' + cts['miRNA35'] + '  3\' --> 5\'' + '\n')
                    f.write('Target: ' + cts['mRNA53'] + '  5\' --> 3\'' + '\n')
                    f.write('\n')
            if number_hits == 0:
                f.write('target site:  no hits found' + '\n')
    f.close()


if __name__ == "__main__":


    start_time = time.time()
    main()
    print("--- %s seconds ---" % (time.time() - start_time))

