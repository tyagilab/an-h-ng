import numpy as np
import pandas as pd
from tqdm import tqdm
import json


"""
This file is to collect the miRNA id and find the relevant strain. The input is from ftp://mirbase.org/pub/mirbase/CURRENT/miRNA.xls.zip and save as miRNA-from-mirbase.xls
miRNA-known-Placenta-labour-plasma.csv provided from Dr Sonika

The output is id_strain json dictionary for further process.
"""

def main():
    dfid = pd.read_csv('miRNA-known-Placenta-labour-plasma.csv')

    dfst = pd.read_excel('miRNA-from-mirbase.xls')
    
    cols = ['Accession', 
            'ID', 
            'Status', 
            'Sequence', 
            'Mature1_Acc', 
            'Mature1_ID',
            'Mature1_Seq', 
            'Mature2_Acc', 
            'Mature2_ID', 
            'Mature2_Seq']
    
    dfst = dfst[cols].dropna()

    for col in cols:
        dfst[col] = dfst[col].apply(lambda x: x.lower())

    dfid['Mature miRNA ID'] = dfid['Mature miRNA ID'].apply(lambda x: x.lower())


    dfst_1 = dfst[['Mature1_ID','Mature1_Seq']].drop_duplicates(subset=None, keep='first', inplace=False)
    dfst_2 = dfst[['Mature2_ID','Mature2_Seq']].drop_duplicates(subset=None, keep='first', inplace=False)

    web_mirbase_dict_1 = dict(zip(dfst_1.Mature1_ID,dfst_1.Mature1_Seq))
    web_mirbase_dict_2 = dict(zip(dfst_2.Mature2_ID,dfst_2.Mature2_Seq))

    mirbase_dict = {} #from Dr Sonika

    for index, row in dfid.iterrows():
        if dfid.at[index, 'Mature miRNA ID'] in list(web_mirbase_dict_2.keys()):
            mirbase_dict[dfid.at[index, 'Mature miRNA ID']] = web_mirbase_dict_2[dfid.at[index, 'Mature miRNA ID']]
        elif dfid.at[index, 'Mature miRNA ID'] in list(web_mirbase_dict_1.keys()): 
            mirbase_dict[dfid.at[index, 'Mature miRNA ID']] = web_mirbase_dict_1[dfid.at[index, 'Mature miRNA ID']]

    with open('id_strain.json', 'w') as outfile:
        json.dump(mirbase_dict, outfile)
    
if __name__ == "__main__":
    main()