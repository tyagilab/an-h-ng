"""
Processing the 3UTRaspic.Hum.fasta to the file readable by DeepMirTar.py

Must divide into multiple file
"""

import pandas as pd
import json
import os
import string
import collections

def main():
    print('processing data....')
    f = open('3UTRaspic.Hum.fasta')
    data = f.readlines()
    f.close()



    for i in range(len(data)):
        if data[i].startswith('>'):
            line = '> ' + data[i].split(' ')[1].replace('>','')
            line = line.strip()
            data_dic[line] = ''
            trim_dic[line[-6:]] = ''
        else:
            data_dic[line] += data[i].upper()
            trim_dic[line[-6:]] += data[i].upper()

    mRNA_list = list(data_dic.items())
    filename = 'mRNA_human/mRNA_human_1.fa'
    fo = open(filename,'w')
    for count in range(len(list(data_dic.keys()))):
        if count % 1000 == 0 and count > 999:
            fo.close()
            filename = 'mRNA_human/mRNA_human_' + str(count//100+1) + '.fa'
            fo = open(filename,'w')
        fo.write(mRNA_list[count][0] + '\n') 
        fo.write(mRNA_list[count][1])
        fo.write('\n')

    with open('mRNA_human.fa','w') as fo:
        for key in data_dic.keys():
            if ('3HSAA072041' not in key):
                fo.write(key + '\n') 
                fo.write(data_dic[key])
                fo.write('\n')

    fo.close()    

    json_data = open('id_strain.json', 'rb').read()
    miRNA = json.loads(json_data)

    with open('miRNA_human.fa','w') as fo:
        for key in miRNA.keys():
            line = '> ' + key
            fo.write(line + '\n') 
            fo.write(miRNA[key].upper())
            fo.write('\n')
            fo.write('\n')

    fo.close()

    #reading aspic file from here, extract mRNA
    df = pd.read_csv('../human_pred/human_predictions_S_C_aug2010.txt', sep = '\t')
    def get_digit(t):
        g = ''
        for s in t:
            if s.isdigit():
                g+=s 
        return g

    # all databases use their own reference codes, so it is unlikely in many cases that last 6 digits are the same as NM_

    gene_set = set()
    ext_set = set()
    
    for index, loc in df.iterrows():
        if (df.at[index, 'gene_symbol'] not in gene_set) \
        and ((df.at[index, 'ext_transcript_id'][:2] == 'NM') or (df.at[index, 'ext_transcript_id'][:2] == 'AF') ) \
        and ((len(df.at[index, 'ext_transcript_id']) == 9) or (len(df.at[index, 'ext_transcript_id']) == 12)): 
            gene_set.add(df.at[index, 'gene_symbol'])
            gene_dic[df.at[index, 'gene_symbol']] = df.at[index, 'ext_transcript_id'][3:9]

    df['digit'] = df['gene_symbol'].apply(lambda x: get_gene_dic(x))
    df['strain'] = df['digit'].apply(lambda x: get_trim_dic(x))
    to_dict = df[df['strain'] != ''][['ext_transcript_id','strain']]

    
    for index, loc in to_dict.iterrows():
        mRNA_dic[to_dict.at[index, 'ext_transcript_id']] = to_dict.at[index, 'strain']

    with open('mRNA_human.fa','w') as fo:
        for key in mRNA_dic.keys():
            line = '> ' + key
            fo.write(line + '\n') 
            fo.write(mRNA_dic[key].upper())
            fo.write('\n')
            fo.write('\n')

    fo.close()
    
    print('Done! Please check miRNA_human.fa and mRNA_human.fa!')
    
    return
        
def get_gene_dic(x):
    if x in list(gene_dic.keys()):
        return gene_dic[x]
    else:
        return ''
    
def get_trim_dic(x):
    if x in list(trim_dic.keys()):
        return trim_dic[x]
    else:
        return ''
    
if __name__ == "__main__":
    gene_dic = {}
    mRNA_dic = {}
    data_dic = collections.OrderedDict()
    trim_dic = collections.OrderedDict()
    record = []
    main()
    