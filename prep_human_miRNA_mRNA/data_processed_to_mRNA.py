"""
Processing the 3UTRaspic.Hum.fasta to the file readable by DeepMirTar.py
This data will need id_strain.json, 3UTRaspic.Hum.fasta, and produce
miRNA_human.fasta, and mRNA_human.fasta
"""

import pandas as pd
import json

def main():
    print('Data processing started...')
    f = open('3UTRaspic.Hum.fasta')
    data = f.readlines()
    f.close()

    data_dic = {}
    record = []

    for i in range(len(data)):
        if data[i].startswith('>'):
            line = '> ' + data[i].split(' ')[0].replace('>','')
            data_dic[line] = ''
        else:
            data_dic[line] += data[i].upper()

   
    with open('mRNA_human.fa','w') as fo:
        for key in data_dic.keys():
            if ('3HSAA072041' not in key):
                fo.write(key + '\n') 
                fo.write(data_dic[key])
                fo.write('\n')

    fo.close()    

    json_data = open('id_strain.json', 'rb').read()
    miRNA = json.loads(json_data)

    with open('miRNA_human.fa','w') as fo:
        for key in miRNA.keys():
            line = '> ' + key
            fo.write(line + '\n') 
            fo.write(miRNA[key].upper())
            fo.write('\n')
            fo.write('\n')

    fo.close()
    print('Data processing done!')
    
if __name__ == "__main__":
    main()