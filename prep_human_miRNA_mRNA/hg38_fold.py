import pandas as pd
import json
import os
import string
import collections
from tqdm import tqdm

def main():
    print('Convert hg38-3UTR-fold.txt to fasta format')
    print('Please wait! Data is being processed')

    df = pd.read_csv('hg38-3UTR-fold.txt', sep='\t')
    
    with open('hg38-3UTR-fold.fa','w') as fo:
        for index, loc in tqdm(df.iterrows(), total=len(df)):
            fo.write('>' + str(df.at[index, '#hg38.foldUtr3.name']) + '\t' + str(df.at[index, 'hg38.kgXref.mRNA']) + '\n')
            fo.write(str(df.at[index, 'hg38.foldUtr3.seq']).upper() + '\n')
            fo.write('\n')
        fo.close()  
    
    print('Done! Please check hg38-3UTR-fold.fa!')
    
    return
    
if __name__ == "__main__":
    main()