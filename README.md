Miniconda3 - to install Anaconda and bioconda, miranda 

ViennaRNA python package https://www.tbi.univie.ac.at/RNA/#download

miRanda software (aug-2010): To search CTS Download: http://www.microrna.org/microrna/getDownloads.do (Make sure that the Miranda program is executable in the Terminal.)

BLAST+ executables: To do sequence alignment. (Problem of Python, must export every time system started PATH=~/il65/user/filename/ncbi-blast-2.9.0+/bin/:$PATH
 
Hunan 3UTR file for human here http://utrdb.ba.itb.cnr.it/home/download. Using miRNA and predict for all mRNA

DeepMirTarV2.0 (Tensorflow and Python 3.7)

The miRNAs files collected from Dr Sonika. 
